package ru.unn.weighr;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите свой вес (кг): ");
        double weight = in.nextDouble();
        System.out.println("Введите свой рост (м): ");
        double height = in.nextDouble();
        double index = weight / Math.pow(height, 2);

        if(index < 18.5) {
            System.out.println("У Вас дефицит массы тела.");
        }
        else if(index > 25) {
            System.out.println("У Вас избыточная масса тела.");
        }
        else {
            System.out.println("У Вас нормальная масса тела.");
        }

    }
}

